Rails.application.routes.draw do
  resources :events do
  collection do
    get 'all'
  end
end
  resources :people
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
