class ChangeEvents < ActiveRecord::Migration[5.2]
  def change
    change_table :events do |t|
      change_column(:events, :date_of_e, :date, null:false)
      change_column(:events, :description, :string, null:false)
      t.references :people, index: true, foreign_key: true
    end
  end
end
