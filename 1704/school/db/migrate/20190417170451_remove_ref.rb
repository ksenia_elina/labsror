class RemoveRef < ActiveRecord::Migration[5.2]
  def change
    remove_reference(:people, :events, index: true)
  end
end
