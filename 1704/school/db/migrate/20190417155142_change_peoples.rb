class ChangePeoples < ActiveRecord::Migration[5.2]
  def change
    change_table :people do |t|

      change_column(:people, :first_name, :string, null:false, limit: 32)
      change_column(:people, :last_name,:string, null:false, limit: 32)
      change_column(:people, :second_name, :string,null:false, limit: 32)
      change_column(:people, :sex,:string, null:false)
      t.references :events, index: true, foreign_key: true
    end
  end
end
