json.extract! event, :id, :date_of_e, :description, :created_at, :updated_at
json.url event_url(event, format: :json)
